---
title: Presentations
layout: page
permalink: /presentations/
---

These are my presentations.

<div class="home">
  <ul class="post-list">
    {% for post in site.categories.presentations %}
      <li>
        <h2>
          <i style="font-size:0.5em;" class="fa {{ post.thumbnail }}"></i>
          <a style="display:inline !important;" class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
        </h2>
		<p>{{ post.summary }}</p>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>
      </li>
    {% endfor %}
  </ul>
</div>
