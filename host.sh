#!/bin/bash
docker run --expose 4488 --rm -it -p 4488:4488 -v $(pwd):/src jekyll jekyll serve --source /src/
