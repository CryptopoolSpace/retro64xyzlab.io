---
layout: post
title: "Introduction To Firejail, AppArmor, and SELinux"
date: 2018-10-16T04:52:43-07:00
author: RetroMe
summary: >
  Introduction To Firejail, AppArmor, and SELinux is a two-hour course 
  designed to provide an overview of three major tools that could be deployed 
  on your server or personal computer to enhance security through greater and 
  more granular control over your applications.
categories: presentations
thumbnail: fa-desktop
tags:
 - selinux
 - apparmor
 - firejail
 - kernel
 - phoenix linux users group
---

* TOC
{:toc}

## Performance Objective

At the conclusion of the course the student will be able to:

1. Identify what the Linux Kernel Security Module Interface is.

2. Identify what sandboxing is.

3. Identify the tools and their use to develop an AppArmor profile.

4. Identify the benefits of sandboxing.

5. Describe what SELinux is and who created it.

## Introduction

A multitude of options exist when you decide to implement a Linux kernel
security module. This breadth of choice can often be overwhelming and so a
greater understanding of the tools and what they accomplish is called for. We
will discuss the most popular kernel level security modules, how they are
implemented, and what they do to better help you decide what to add to your
system.

## Linux Kernel Security Modules

The LSM or Linux Kernel Security Modules implement all of the tools necessary
to provide mandatory access control. AppArmor and SELinux are two major tools
that implement and adopt this method of access control.
An [excellent paper][lsm_whitepaper] is reproduced here without permission.

The LSM inserts hooks at every point in the kernel where a user can conduct a
system call to import kernel objects. This includes inodes and task control
blocks. This is done with a narrow scope that is intended to avoid large and
complex change patches.

## Mandatory Access Controls (MAC)

Mandatory access control is a method by which an operating system limits access
to or performance of an object or action. Generally this means that requests
for processes, threads, files, ports, or devices are managed based on a
specific set of constraints that are tested against authorization rules
(policies).

### AppArmor

AppArmor is a Linux Kernel Security Module that allows the user to restrict
programs with the use of a profile specific to that application. These profiles
are designed to delegate capabilities that include sockets, network access,
file access, and more. Canonical, creators of Ubuntu, are known to be
contributors to this security tool. AppArmor is implemented through the LSM
kernel interface. AppArmor is an excellent alternative to the much more
difficult to setup SELinux.

#### Overview

Install is simple. If you are using Arch or a variant of Arch you can run -

```
$ sudo pacman -S apparmor
```
You will need to edit `/boot/syslinux/syslinux.cfg` using `sudo vim` and locate
the APPEND line. You can then add 

```
APPEND apparmor=1 security=apparmor
```

Some distributions come with AppArmor preinstalled. Ubuntu is one of those.

#### History

AppArmor began as an application known as SubDomain for the now defunct Immunix
OS. Eventually Novell acquired Immunix and rebranded the SubDomain application
as AppArmor. They then began the process of porting the application to Linux.
SUSE later became the legal owner of the trademarked name AppArmor. AppArmor
was ported to Ubuntu in 2007.

#### Policies

The [Arch Linux Wiki][archwikiapparmor] has a comprehensive explanation on how
to setup AppArmor as well as the policies needed to get it running. One of the
AppArmor developers has an excellent [introduction to using AppArmor][apparmorguide]
on his website.

#### Features

AppArmor provides MAC functionality as a supplement to the traditional
Discretionary Access Control or DAC within Linux. Using these tools we can
create and deploy AppArmor profiles that
restrict access for our processes.

#### Use cases

AppArmor can be used to reduce the threat footprint of specific applications.
AppArmor comes with an example profile that locks down Firefox. Any application
could be theoretically enhanced through the use of an AppArmor profile.
Developers should consider creating AppArmor profiles for their applications.

### SELinux

SELinux provides a flexible Mandatory Access Control system that is built
directly into the Linux kernel. SELinux defines the access and transition
rights of every user, application, process, and file on the system. This is a
greatly honed method over the traditional Discretionary Access Control in which
an application or process runs as a UID or SUID and inherits the users
permissions to all objects available to a user. SELinux protects a system from
malicious or flawed applications by limiting the access to files, sockets, and
other processes that the application can access.

The method by which SELinux functions is very simple. A process will perform an
action request. It may ask to read a file. This request is pushed to the
SELinux security server which then checks an access vector cache that stores
subject and object permissions. If it does not see this request there it will
then consult the SELinux Policy Database. If the policy is found it will then
make a decision of Yes or No for the access request. If yes, the object
requested is returned. If no, an AVC denied message is generated and the object
is not returned.

A [detailed manual][rhatselinpdf] (PDF) for SELinux is available from RedHat. I
have produced a mirror [here][rhatselinpdfrepro] (PDF).

#### Overview

The [SELinux Github][githubselinux] is the curent canonical location for
continued development of SELinux proper. The [NSA][nsaselinux] has defined
SELinux as -

	NSA Security-Enhanced Linux is a set of patches to the Linux kernel and
	utilities to provide a strong, flexible, mandatory access control (MAC)
	architecture into the major subsystems of the kernel. It provides an enhanced
	mechanism to enforce the separation of information based on confidentiality and
	integrity requirements, which allows threats of tampering, and bypassing of
	application security mechanisms, to be addressed and enables the confinement of
	damage that can be caused by malicious or flawed applications. It includes a
	set of sample security policy configuration files designed to meet common,
	general-purpose security goals.

SELinux was born from the [Information Assurance Mission][nsaiad] of the NSA.
The NSA serves an important role in not only ferreting our information but also
safe guarding it. Their directives means that they are duty bound to partner
with government, industry, and academia to defend our nation. 

##### Important Note About NSA-IAD

	Note: The IAD.Gov website uses TLS 1.2, supported by a Department of Defense
	(DoD) PKI certificate, to ensure confidentiality and integrity for all users.
	IAD.Gov website users will need to have the current DoD Root and Intermediate
	Certificate Authorities (CA) loaded into their browsers to avoid receiving
	untrusted web site notifications.

This means that no browser trusts the SSL certificate for their IAD site and
you must install a DoD certificate to get their site to work under some
browsers.

#### History

SELinux was originally a project created by the National Security Agency with
the assistance of others. SELinux is an implementation of the Flask
architecture for operating system security. Flask architecture uses MAC as an
administratively defined security polic to control all subjects and objects
while basing all access decisions on set policy. Flask is a least privilege
method for task rights.

SELinux has been integrated into the Linux kernel as a LSM framework. The
original SELinux implementation was not scalable and was not originally
supported by the Linux Kernel. The second interation of SELinux was a loadable
kernel module but it has issues with scaling and support of file systems. The
third implementation has full support for LSM and better support for file
systems. This iteration has been a joint effort between the NSA, Red Hat, and a
community of SELinux developers.

#### Policies

You can [download][nsaguideselinux] the NSA guide to SELinux that I have
reproduced without permission.

#### Implementations

SELinux is extremely powerful and provides extremely granular control but this
level of control breeds complexity and that makes it much harder to learn to
use correctly. AppArmor is simpler and requires a less steep learning curve to
overcome.

#### Use cases

RedHat and Centos systems like those deployed in most government related data
centers will use SELinux.

## Comparisons Between AppArmor and SELinux

AppArmor is easier to implement, simpler to understand, and enjoys the benefit
of being better explained. SELinux is less transparent and due to its history
of being linked to Operating Systems with less installation among home users,
it is much more difficult to find assistance with. Either application will
provide MAC and can be deployed as you see fit. At this time I recommend
AppArmor due to the simplicity in which it is managed and deployed.

## Sandboxing

There exist several methods of sandboxing an application. You can practice full
system virtualization with something like VirtualBox, you can use an
alternative method like Docker, or you can use a tool like Firejail that
provides a SUID method of sandboxing. No matter what choice you make, you must
understand that they each have their own benefits and weaknesses.

### FireJail

[Firejail][firejail] is a SUID program that reduces the risk of security
breaches by restricting the running environment of an application using Linux
namespaces in conjunction with seccomp-bpf. Firejail exposes tools built
directly into the Linux Kernel in order to allow a process and all process
descendants to have their own private view of a globally shared Kernel
resource. This includes the network stack, process table, and the mount table.

Firejail is written in C and has no dependencies. Since the entire application
is simply implementing tools available in the Kernel you will find nearly no
impact to the speed of applications and very little resource overhead. Firejail
can be used with a wide array of applications and provides sandboxing for
everything from web browsers to web servers and all the tools in between.

Firejail can be used in conjunction with AppArmor. However it is important to
note that some features of AppArmor are only available in Ubuntu distributions
at this time. There is talk that this limitation may change in the coming year
and so this notation may be out of date at some time.

## Answers

1. The Linux Security Module Interface is a framework for allowing the Linux
   kernel to support many different computer security models without
   advertising for or prioritizing one over the other. The currently accepted
   modules in the official kernel are AppArmor, SELinux, Smack, TOMOYO Linux,
   and Yama.

2. A sandbox is a security mechanism for collecting resources and providing
   them to an application without allowing that application to modify or
   otherwise access resources outside the agreed upon isolated environment.

3. AppArmor profiles can be developed through the use of the AppArmor Audit
   Framework. Commands include auditctl, ausearch, and aureport.

4. Sandboxing can protect a system from malicious code by preventing access to
   important system files, memory, or the mechanisms necessary to write
   permanent changes to the host operating system.

5. SELinux is a Linux kernel security module for managing access control
   policies created originally from projects developed by the United States
   National Security Agency.

## Conclusion

No operating system is perfect. While Linux does provide enhanced security over
the competition, it can still be improved. You as a user must make the decision
based on your personal threat profile whether tools like AppArmor, SELinux, or
Firejail could enhance your security. While the fine tuned control provided by
AppArmor or SELinux within the MAC in the Linux Kernel could enhance your
safety, it is also possible that the increased complexity could present other
avenues of attack.

I recommend that any one installs Firejail and deploys this tool. Regardless of
your need for a MAC, I cannot sing the praises of Firejail enough. This is a
simple tool that can be deployed on many common applications. If you are
running a web browser, you need to be running Firejail in conjunction. I cannot
recommend it enough.

## Final Recommendations

1. Use Linux.
2. Implement safe security practices.
3. Understand what your operating system provides.
4. Be a good neighbor. 

[githubselinux]: https://github.com/SELinuxProject 'The SELinux Project Github'
[nsaselinux]: http://archive.is/YxHBZ 'The NSA website for Security-Enhanced Linux'
[nsaiad]: https://www.iad.gov/iad/index.cfm 'The NSA Information Assurance Mission Site'
[rhatselinpdf]: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/pdf/selinux_users_and_administrators_guide/Red_Hat_Enterprise_Linux-7-SELinux_Users_and_Administrators_Guide-en-US.pdf 'The Red Hat Guide to SELinux'
[rhatselinpdfrepro]: /../assets/pdf/RHatSELinuxAdminGuide.pdf 'The Red Hat Guide To SELinux Reproduction'
[lsm_whitepaper]: /../assets/pdf/lsm_whitepaper.pdf 'Linux Security Module Framework Paper'
[archwikiapparmor]: https://wiki.archlinux.org/index.php/AppArmor#Installation 'AppArmor from the Arch Wiki'
[apparmorguide]: http://archive.is/5A8Ka 'A comprehensive guide to AppArmor'
[nsaguideselinux]: /../assets/pdf/nsaselinuxguide.pdf 'An NSA primer on SELinux'
[firejail]: https://firejail.wordpress.com/ 'Firejail Security Sandbox'
