---
layout: post
title: "Introduction To WiFi Hacking"
date: 2019-10-29T04:52:43-07:00
author: RetroMe
summary: >
 Introduction To Wireless Network Vulnerabilities is a gentle introduction to
 the hardware and software that creates the invisible world of wireless
 networks.
categories: presentations
thumbnail: fa-desktop
tags:
 - wifi
 - penetration testing
 - exploits
 - phoenix linux users group
featured_video: 'https://www.youtube.com/embed/pkvlkFEhCSQ'
---

* TOC
{:toc}

## Performance Objective

At the conclusion of the course the student will be able to:

1. Identify one site that can assist you in locating wireless networks.

2. Identify one tool used to crack wireless networks.

3. Identify what frequency WiFi functions on.

4. Identify a tool for phishing WiFi passwords. 

## Introduction

Wireless Fidelity, or WiFi, is a type of technology employed to provide
connectivity to a computer network without cable or hardwired connection. WiFi
works within the 2.4Ghz or 5Ghz range and should not interfere with cellphones,
broadcast radio, television, or hand held radios. WiFi functions by
transmitting data over radio waves between a client device and a device called a
router. This router can then transmit data to systems internally or outwards to
the internet.

WiFi is used across the planet to simplify the deployment of networks, provide
communication to large numbers of devices, and to increase convenience. While
WiFi is well known, there are additional wireless signals that we may not
consider or think about regularly due to their ubiquitous nature. This includes
cell phones, blue tooth, and other wireless devices.

WiFi is neither more nor less secure than a traditional hard wired network but
is instead a completely different and less comparable interface. Wired and
wireless both provide a method for devices to communicate but will require
separate thought processes when it comes to security, availability, and
functionality. You can not treat one network the same as the other or expect
them to behave the same.

#### WiFi does not make you sick.

Extensive research has concluded to date that WiFi does not make you sick or
cause autism. However, there is a growing number of researchers who believe
that the 'nocebo' effect can cause illness. Fake WiFi devices were strapped to
human test subjects who began to complain of anxiety, head pain, and tingling
feelings. The devices were completely harmless and fake but the human mind was
capable of causing users to feel they were experiencing harm.

## WiFi Frequencies

WiFi versions or iterations have changed over the years. WiFi began as an 11
megabit per second method for networking devices but today we now see multi
gigabit speeds. Each iteration has brought about numerous changes and a gradual
increase in performance as well as some changes in security. 

#### 802.11b

Transmits at 2.4GHz and moves data at 11 megabits per second. Released at the
same time as amendment a. 2.4GHz easily experiences interference.

#### 802.11a

Transmits data at 5GHz. Transmits at a maximum of 54 megabits per second. Uses
OFDM or Orthogonal Frequency Division Multiplexing to enhance reception by
dividing the radio signals into smaller signals before reaching the router.

#### 802.11g

Transmits at 2.4GHz. Moves data at 54 megabits per second. Uses OFDM technology.

#### 802.11n

Transmits at 5GHz and 2.4GHz. Moves data at 140 megabits per second with a
theoretical max of 300 Mbps (Maybe 450 with MIMO).

#### 802.11ac

Transmits at a theoretical maximum of several Gigabits per second and works
exclusively in the 5 GHz band. Uses beam forming and focuses transmission of
signals directly at devices. Supports Multi User MIMO to increase throughput of
devices and allows several devices to communicate at roughly at the same time.

## WiFi Security

The most important thing to remember is that WEP or Wired Equivalent Privacy
was released in 1997 and is completely broken. WEP can be broken in minutes or
less. Your modern devices will neither offer nor function with a WEP network
and most routers no longer support it. Stay far away from WEP.

### WPA-TKIP and WPA2-AES

WP or WP-TKIP was an intermediate fix while working towards WPA2. TKIP is
considered fairly vulnerable and should be avoided or turned off. As of this
writing you should be using WPA2-AES as this is considered the most secure
method of security. 

## WiFi Channels

US Routers have 11 channels at 2.4GHz and 45 channels for the 5GHz networks.
Adjacent channel interference occurs when devices from overlapping channels
broadcast over each other. The 2.4GHz spectrum has three channels that do not
technically overlap. Channels 1, 6, and 11.

## WiFi Aggregation

Your wireless router may support packet aggregation. If it is using 801.11ac it
is mandatory. The concept of packet aggregation is simple. You send two or more
data frames in a single transmission. This reduces overhead and increases
performance. This reduces the overhead of sending many smaller packets that
could reduce performance.

## WiFi SSID Setup

Should I hide my WiFi SSID? What is an SSID?

An SSID or Service Set Identified is the beacon that informs individuals in the
area that a wireless network exists and can be connected to. Broadcasting the
SSID makes it very simple for individuals interested in connecting to a network
to find it. However, it is important to understand that obscuring the SSID or
otherwise hiding it does not provide any form of security. 

There are multiple methods by which you can decloak or locate a hidden WiFi
networks and reveal the name. It is considered a trivial action. So when should
we hide our SSID name? When we wish to declutter the local airwaves is
generally the only time it is necessary or relevant. It should not be
considered a security feature or tool but instead a tool of convenience.

## Aircrack-ng

[Aircrack-ng][acng] is a complete suite of tools to assess WiFi network
security. ACNG provides monitoring, attacking, testing, and cracking tools.
Aircrack provides the tools necessary to put your wireless cards into monitor
mode using the `airmon-ng` command.

Aircrack-ng is a fantastic tool for testing the security of your network and
conducting attacks on WEP as well as WPA2 if you wish to run a dictionary
attack.

## Pixiewps

Pixieswps is a tool you can use for brute forcing WPS pins. WPS is designed in
such a way that it is possible to essentially attack approximately 11,000
combinations in order to get the correct code. This requires approximately 12
seconds to execute and quickly defeats WPS and therefore can assist in
defeating WPA.

Competitive tools like Reaver claim attack times in the hours but Pixiewps and
the 'pixie-dust attack' are capable of defeating WPS and recovering WPA
credentials in seconds. Pixiewps is a tool that was born from a collaboration
that began on the Kali Linux web forums among active members of the community.

## Kismet Wireless

[Kismet][kismet] is a wireless network and device detector, sniffer, wardriving
tool, and wireless intrusion detection framework. 

### How To Find A Hidden SSID

With [aircrack-ng][git-aircrackng] -

```
$ sudo pacman -S aircrack-ng
$ sudo airmon-ng start wlan0
$ sudo airodump-ng mon0
$ sudo airodump-ng -c 1 --bssid XX:XX:XX:XX:XX:XX mon0
$ sudo aireplay-ng -0 15 -c CLIENT BSSID -a NETWORK BSSID mon0
```

## Wifiphisher

The number one way to conduct an attack on a user and to gain access to their
system is to access for that access. Wifiphisher is a fantastic tool for
conducting that style of attack. It allows you to create a fake Network Manager
Connect Page, Firmware Upgrade Page, OAuth Login Page, or Browser Plugin Update
page and force users to see this on connection to the rogue access point.

### How to use Wifiphisher

```
$ yay -S wifiphisher
$ sudo wifiphisher -i URLAN1
```

Wifiphisher is easy to install and use and simply requires `sudo` access and an
ability to follow a prompt.

### WPS and Wifite

Attacking [WPS with Wifite][crackwpswifite] is relatively simple. You run a
WiFi device in monitor mode, choose your target, and wait.

### WPA2 and Wifite

```
$ wifite -mac -aircrack -dict /usr/share/wordlists/passwords.txt
```

## WIDS with Kismet

Kismet provides a method by which you can detect rogue wireless access points,
monitor the local area, and alert. Creating or purchasing a wireless intrusion
detection system will greatly enhance the safety of your network and users.

Users will setup their own methods to avoid road blocks and will often do so
with little regard to the sanctity of the network or security in general. You
may also have users who have learned to conduct a Denial of Service and now
revel in causing disruption. You need to be able to look for and identify noise
and congestion.

You may also have intruders or attackers who want access to your network to
cause harm. They may be looking for payment data, Personally Identifiable
Information, or methods by which to pivot to other resources. An attacker may
compromise a network in order to attack the clients of that company. External
attacks can be devastating but worse yet is the internal attack.

It should also be noted that WIDS are not perfect and it can be exceptionally
difficult to know what is or is not authorized on your network. Bring Your Own
Device is changing how we work and alleviating costs but increasing
vulnerabilities on the network. A WIDS may not be able to identify or
fingerprint many vulnerabilities. You must also have plenty of space and the
capability of monitoring trends over time or else you will not be able to see
what is really happening. A single packet event will rarely reveal an issue.

## Wave Bubble

Users can construct or purchase a [wave bubble][wavebubble] and have the
capability of jamming a large amount of the RF spectrum. This will include
cordless phones, GPS, wifi, bluetooth and potentially more within a 20 foot
radius.

It should be noted that documented uses of devices like the wave bubble within
the United States is relatively sparse. However, in countries like South
Africa, similar tools are deployed during perpetration of violent crime. A man
portable backpack sized jammer is regularly deployed by criminals who can jam
communications up to nearly a half mile. After jamming locals communications
the criminals will then conduct an attack.

Unconfirmed reports of American discussion of this tactic among potential
threat actors has occurred online on varying discussion sites for these groups.
The idea that an individual could deploy a tool like this in an ambush setting
or during a home invasion has been shared in a manner similar to what is
happening over seas.

## GNU Radio

GNU Radio is a free and open source tool that provides signal processing blocks
to implement software defined radio. Low Cost and easily available RF hardware
can be deployed to assist in hobby, academic, and even commercial environments.

GNU Radio performs signal processing. You can work with GNU Radio without
writing code but it is recommend that you have the ability to work comfortably
in a language like python in order to get the most out of this tool.

## Wigle

[Wigle][wigle] is a website and mobile tool that is used for creating physical
maps of where wifi networks are located. Wigle tracks approximately 600,000,000
networks and reports on them using reporters who help locate these items.

It is also possible to use Wigle to locate cellular towers which introduces a
different set of issues. Russia is working on installing Pole-21 anti missile
jamming systems on their various civilian cellular network towers. While groups
within the United States have been discussing the feasibility of attack
critical infrastructure like communication towers and the like.

Wigle is simple to use and allows you to look for wireless access points based
on vicinity. This can reveal whether or not someone is in an area or operating
in an area. Wigle is an excellent tool for conducting reconnaissance on a site
remotely.

## Answers

1. Wigle is a site that can be used for locating wireless networks.

2. Aircrack-ng can be used to crack wireless networks.

3. Wifi normally functions on 5GHz or 2.4GHz.

4. Wifiphisher (The Rogue Access Point) is a tool that can be used for phishing
   wifi passwords. 

## Conclusion

Securing your wireless network goes beyond detecting attacks. A good network
engineer knows what devices are authorized, where they are located, and what
they should be doing. WiFi removes boundaries and with the advent of 'Bring
Your Own Device Culture' it is becoming increasingly difficult for admins to
know what is or is not on their network.

Finding a hidden SSID is trivial. Basic computing tools can be used to defeat
any obfuscation. Rogue access points, targeted attacks, and even phishing can
be deployed to defeat most basic forms of securing a wireless network.

You must consider implementing a Wireless Intrusion Detection System and treat
your wireless networks as potentially vulnerable at all times. In addition, you
should consider deeply how a WiFi network can reveal additional meta data about
a building, person, or surrounding area. Looking for WiFi and other wireless
signals can reveal tremendous amounts of information about an area and the
activities being conducted there. SIGSEC or Signals Security matters.

## Final Recommendations

1. Choose *nix. 
2. Secure your network and monitor local traffic.
3. Regularly review tools like Wigle for changes in your local area.
4. Develop your equipment and gear.
5. Choose freedom.

[git-wifiphisher]: https://github.com/wifiphisher/wifiphisher 'Rogue Access Point Framework'
[git-aircrackng]: https://github.com/aircrack-ng/aircrack-ng 'WiFi securiting auditing tools'
[acng]: https://www.aircrack-ng.org/ 'Aircrack-ng Full Suite'
[gnuradio]: https://www.gnuradio.org/ 'Free and Open Software Radio Ecosystem'
[kismet]: https://www.kismetwireless.net/ 'Kismet wireless network detector'
[bgbdecloak]: https://archive.is/BgSyt 'Decloaking Hidden WiFi Networks'
[wigle]: https://www.wigle.net/ 'All the networks. Found by Everyone.'
[pixiewps]: https://github.com/wiire-a/pixiewps 'Pixie Dust Attack'
[wavebubble]: http://www.ladyada.net/make/wavebubble/index.html 'The Wave Bubble'
[crackwpswifite]: https://archive.is/BSVyo 'How to crack wps with wifite'
