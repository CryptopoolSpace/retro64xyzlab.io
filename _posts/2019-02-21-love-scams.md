---
layout: post
title: "Introduction To Love Scams - Supplemental Discussion"
date: 2019-02-20T04:52:43-07:00
author: RetroMe
summary: >
  Introduction To love scams is a supplemental explanation of how scammers take
  advantage of individuals who are lonely or otherwise gullible and willing to
  part with their money in the hopes of acquiring love. The love scammer often
  preys on a very vulnerable population while pretending to be a type of person
  that someone may care for. Here are tips on how to spot scammers  and their
  methods. 
categories: presentations
thumbnail: fa-desktop
tags:
 - phoenix linux users group
 - supplemental discussion
 - theft
 - financial crime
 - scams
---

* TOC
{:toc}

## Performance Objective

At the conclusion of the course the student will be able to:

1. Describe what a love scam is.

2. Identify a type of person or profile that a love scammer may pose as.

3. Identify what types of people are scammed.

## Love or Romance Scams

Criminal networks and individuals are working overtime to part love struck
victims from their money. While many individuals are spending the time leading
up to February looking for someone to keep them company during Valentines day,
love scammers are looking for desperate victims who are willing to feed their
bank account.

### Methods

A scammer could potentially use any method available to them to bilk a victim
out of their money. It is a good idea to familiarize yourself with the common
types of love scams and their methodology. Understanding what scammers do to
accomplish their mission can help you identify otherwise unknown scams and
better defend yourself. The stakes can be [high][onemil].

#### Blackmail

Scammers often look for vulnerable or otherwise weak individuals who they
attempt to convince to indulge in their fantasies while recording or otherwise
monitoring them. This is often an invitation by the scammer to get the victim
to perform sexual acts on camera.

Once a scammer has recorded their victim they will often find their social
media contacts and threaten to send the video to their employers, friends, and
family. The victim usually has to pay a sizable sum to protect their dignity
but the scammer will often still reveal the content or sell it on sexually
explicit sites even after having been paid.

#### Pro-Dater

The pro-dater preys on wealthy foreigners who are willing to visit their
country. They will meet their victim and then orchestrate the victim spending
large amounts of money. This usually requires the help of people who are on the
take.

The foreigner will often be taken by an expensive taxi, shuttled to stores,
convinced to spend money, and then they are either sent away or otherwise left
somewhere with encouragement to send more money when ever they can. Sometimes
they are [murdered][vicmurd].

#### 419 Scams

A 419 scam is similar to most Nigerian wealth scams. The victim is told that
vast wealth is available if they are willing to marry someone. The scammer then
convinces the victim to send money and pay massive fees and bills, stringing
them on, with the promise of marrying and bringing along massive amounts of
wealth.

Claims that victims have been beat, robbed, and even murdered after traveling
to [Africa][samurd] to meet their scammers have been made.

#### Military Impersonation

Many scammers impersonate [military members][poster], preying on their victims by
convincing them that their military service prevents them from visiting. The
relationship will usually progress until the scammer finds an excuse to request
cash. This has lead to victims loosing tens of thousands of dollars as they
believe they are assisting a member of the armed forces whom they 'love'.

### Defense

Slow down. Stop. Time is on your side. Your online love you have never met who
is demanding cash right now is pressuring you in the hopes of making you make
mistakes. The scammer will often profess their love and then demand cash to
help themselves, family members, or for some other issue. Don't rush. A
manufactured emergency is a fast way to get you to take action before you
think.

Don't wire money or use gift cards. Untraceable money transfer funds will
vanish and you will not recover your losses.

Any photo provided to you should be entered into [google image search][gimg] as
a reverse search method.

## Resources

1. The [FBI][fbilove] provides excellent resources on love scams.

2. The [CID][cidlove] provides excellent resources on love scams.

## Answer

1. A love scam is a scam that relies on convincing a person to part with their
   money because of some form of emotional or sexual attachment.

2. Scammers often pose as members of the military because it provides them an
   excuse for being out of the continental United States.

3. Gullible or otherwise desperate people are preyed upon because of their
   strong desire for fulfillment and a willingness to over look issues to
   satisfy their own fantasies.

## Conclusion

The British Psychological Society claims that people who are sensitive or
emotionally stunted are more likely to fall for online dating scams. Older
people, less attractive people, and persons who receive less attention are more
likely to be willing to suspend belief when highly attractive persons provide
them attention.

Be cautious with your social media. People look for people who are older and
present a willing to trust. They specifically look for trust indicators like
strong religious predisposition or posts with positive affirmations. Do not
make yourself an easy mark.

The internet has made scamming a simple career and taken the necessity to be
charismatic out of the equation. Do not allow yourself to be duped or to over
look red flags because you want to prop up your ego. Men and women can both be
victims of the love or romance scam and the only way to protect yourself is to
use caution and skepticism.

[gimg]: https://images.google.com/ 'Image Search'
[romwiki]: https://en.wikipedia.org/wiki/Romance_scam 'The wikipedia'
[fbilove]: https://archive.fo/CUGu2 'FBI scam resources'
[cidlove]: https://archive.fo/oKDdj 'DOD love scam resources'
[poster]: https://archive.fo/X2Ska 'The poster child for military love scams'
[vicmurd]: https://archive.fo/70v51 'Victim murdered' 
[onemil]: https://www.palmbeachpost.com/news/love-lake-worth-woman-gave-online-stranger-million-lost-all/vzsj9kbiO9d3g0JKJCal2O/ 'Expensive'
[samurd]: https://archive.fo/tBGge 'South Africa Murder'
