---                                                                             
layout: post
title: "Book Study - The Prince"
date: 2018-12-30T15:52:43-07:00
author: RetroMe
summary: >
  The Prince is a book on leadership penned in the 16th century for Lorenzo Di
  Piero De Medici. The intention of the book was to either educate one on how
  to survive as a leader of men or to sabotage the rule of those who read it,
  depending on whose opinion you lend credence to.
categories: books
thumbnail: fa-book
tags:
 - book review
 - law enforcement
 - leadership
---

### A foreword

The Prince is a treatise by Niccolo Machiavelli that was penned in the 16th
century on politics. Today, many people are introduced to this book by the
rapper Tupac, who read The Prince in while serving a jail sentence. Tupac
referred to himself as 'Makavelli', a play on 'Machiavelli' who penned the
famous book. Since The Prince is considered a staple reading for anyone
interested in leadership, regardless of the theory it may have been satire or
written to root out monarchists and sabotage them, I think it holds a lot of
interesting information that provides a look into how modern man has not
changed much from his 16th century counter parts.

## The Prince

**To the great Lorenzo Di Piero De Medici**

## Types of principalities - gaining them and holding them

Machiavelli, the author, provides very simple definitions of what constitutes a
state, how they are acquired, and how to hold them. He provides four methods by
which one can gain control over a princedom. The best method of leadership is
to install a princedom in a new acquisition and to take hold of his lands by
living among the people there. Machiavelli believed that taking ownership and
sharing in the fate of the people would make you most likely to grab and hold
your acquisition. However, you can also allow others to control your interests
if you do not let them gain too much power, you can destroy all the powerful
people and prevent them from regaining a foothold, or busy yourself in trying
to prevent foreign powers from gaining a foothold in your lands.

The first choice, to take ownership of a situation and to demonstrate your
shared responsibility is a very common leadership trait. You shouldn't hope for
a better tomorrow but instead should get in the trenches and work towards your
goal in a visible manner. The idea of being visible is very important because
people are less likely to understand or even identify secret works but will
find pleasure in knowing what their leaders are doing.

## Soldiers and war

You should take care not to rely on mercenaries. I think we see this in the
hatchet men of business today. A company will go out and hire from outside
sources in the hopes of improving the company but instead find only temporary
and short term benefits before the company implodes. We have seen this in the
American economy as we have moved to hiring foreigners to do everything from
manage our medicine to control our production and financial data. Our reliance
on mercenaries to manage for us is becoming the same downfall that Machiavelli
warned of in the 16th century. 

## The qualities of princes

If we substitute the word 'prince' for 'leader' we will find that many of the
items discussed in the book have parity with more modern leadership books. I
wonder if the qualities of a prince are the same as that of the modern leader,
or do many modern authors simply regurgitate what is provided by Machiavelli
but with different veneers. We have books about Navy SEALS, Firemen, Police
Officers, and Businessmen and all the things they have learned about leading
others but every book I have read on leadership thus far is a redressed version
of "The Prince".

## The prince's supports

A leader should be cognizant of who speaks to them. The author urges the prince
to be wary of 'yes men', unsolicited advice, and any one who might ask the
prince to remain neutral in a conflict. I think this is sound advice. The more
powerful you are, the more likely you are to be beset by others who wish to
increase their station by giving you the advice you want to hear.

Machiavelli advises that the prince should consider setting a strict chain of
command while rewarding those who give real and sound advice, even if that
advice is contrary to the desires of the prince. Reward good advice.

# Conclusion

A good leader must possess many qualities and no leader will possess all of
them. However, the prince as a book lays a good framework for good leadership.

1. Take ownership and be present. On site leadership will always trump phoning
   it in.

2. Take caution in who you trust. Every one is out to get something.

3. Imitate the greats and emulate the best.

4. Good leaders will always aspire to greatness so take caution in how you
   choose your subordinates. A good leader will aspire. A bad one will ruin
   you. Either can be disastrous if you don't pay attention.

5. Read and study and learn.

6. Practice and prepare. Leaders always war game and build contingencies for
   the 'what if'. Play 'what if' constantly.

7. If you have to punish or be cruel, do it in a way that leaves no room for
   reprisal. 

8. Perception matters. Don't allow others to make assumptions, but instead show
   clearly your accomplishments and the accomplishments of those beneath and
   above you.

9. Don't take the property of others. Machiavelli says men are more likely to
   forget the murder of their father than the loss of their inheritance.

10. Ignore flatterers and 'yes men'. Don't fall victim to your own success.

11. Old offenses are never forgotten. Don't forget who you have wronged. They
    will be back.

12. Receiving benefits makes others feel connected to you. Allow others to
    praise and support you.

13. Trust the ones who stick with you even when times are bad. Reward loyalty.

I believe that 'The Prince' by Machiavelli is a fantastic book that should be
read by anyone interested in leadership or otherwise developing their
understanding of people. The lessons in the book are timeless and apply today
just as they did so many years ago. The life of a Prince is not so different
than the life of a leader today. You lead men, you budget, and you make life
and death decisions in my industry.

I think the main takeaway from this book is that we are the masters of our own
destiny and the only way to succeed is by being bold and dedicating ourselves
to continuing education.

You can purchase the book on [Amazon][amazon].

[wiki]: https://en.wikipedia.org/wiki/The_Prince 'Wiki: The Prince' 
[amazon]: https://www.amazon.com/Prince-Dover-Thrift-Editions/dp/0486272745 'The Prince on Amazon' 
