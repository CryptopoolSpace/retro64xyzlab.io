---
layout: post
title: "Introduction To Assembly Language - Part Two"
date: 2019-06-01T04:52:43-07:00
author: RetroMe
summary: >
  Introduction To Assembly Language part two is the conclusion to my reverse
  engineering course introduction. This is part two of a multi part educational
  series on lower level software development and reverse engineering. Many
  developers and security experts are not familiar with assembly language or
  the lower levels of their hardware. This course is intended to begin
  familiarizing you with assembly language in order to build the foundation for
  future courses on reverse engineering.
categories: presentations
thumbnail: fa-desktop
tags:
 - phoenix linux users group
 - assembly
 - programming
 - reverse engineering
---

* TOC
{:toc}

## Performance Objective

At the conclusion of the course the student will be able to:

1. Understand how to locate values in a running application using scanmem.

2. Understand how to manipulate memory during run time.

3. Identify a tool that can be used to automate malware analysis.

4. Identify one method by which a VM can be identified by malware.

5. Identify one reason for analyzing malware.

## Introduction

Programming at a low level requires intimate knowledge of your hardware.
Understanding how information is stored in ram, accessed by applications, and
eventually disposed of or written to permanent storage is important. We will be
using an application called scanmem in conjunction with the video game Doom to
gain a better understanding of how values can be stored by an application and
located or manipulated by the user. While we will be using a video game for
demonstration purposes it is important to understand that these concepts are
applicable in a wide array of situations.

## Installation

You will need the `scanmem` program for these exercises. We will be
manipulating the video game Doom to better understand how memory can be
accessed and manipulated in a running application.

### Scanmem

```
$ sudo pacman -S scanmem
```

[Scanmem][scanmem] allows us to locate and modify variables in an executing
process. The scanmem application is an interactive debugging utility that can
be used to isolate the address of a variable in an executing process by
successively scanning the process address space while looking for matching
values.

### GZDoom

```
$ yay -S gzdoom
```

[GZDoom][gzdoom] is an application that can be used to play the video game
Doom. Doom is a 1993 first person shooter made by id Software. Doom has
frequently been cited as one of the most important and greatest games of all time.
Today, GZDoom provides a simple method to run this family of games via packaged
files known as WADS.

## Basics

1. Start GZDoom

2. Load Doom

3. Open a terminal and type `pidof gzdoom` or optionally you can run `pgrep gzdoom`

4. Run `sudo scanmem 4865`

5. Type the value we are search for and hit enter while modulating the value
   until you locate it.

You can then SET the value and you will see a result like 
`info: setting *0x56243f28fe88 to 0x3e7...` that will reveal you have
successfully changed your value.

Finding, reading, and changing values in memory is an iportant aspect of
debugging as well as learning how applications function.

### pgrep VS pidof

pgrep can match based on regular expressions but pidof is very literal. You can
use either to accomplsih your goal of getting the pid for an application.

### What is RAM?

RAM is an acronym for random access memory. RAM is the storage place for data
that the CPU or central processing unit is working with. RAM is used for
storing temporary data that will normally be flushed on shutdown or reset of
the computer.

### 32-bit versus 64-bit

A 32-bit CPU can address a maximum of 4,294,967,296 bytes of RAM. This equals
approximately 4 gigabytes. However, in practice the CPU will generally only be
able to use around 3.5 gigabytes because some of the RAM will need to be
reserved for storing temporary values besides memory addresses.

A 64-bit CPU can theoretically address 18,446,744,073,709,551,616 bytes, or
17,179,869,184 gigabytes (16 exabytes) of memory. A 64-bit computer can
certainly access more than 3.5 gigabytes of RAM.

In theory a 64-bit processor is more efficient and capable of operating at much
higher speeds than a 32-bit system because of the ability to address more ram
and to use processor operations for 64-bit code that would not be available to
32-bit users.

## Malware Analysis

Malware analysis is an integral part of the intelligence tool chain that can
reveal who is targetting your organization, how they are doing so, and
potentially what tools they may be using. It is vital that we each contribute
to the knowledge base by becoming familiar with automated tools for analyzing
these attack vectors. Tools like Cuckoo can be deployed in a segregated network
with minimal effort and they can be used to gather information that can be of
importance to law enforcement, investigators, and others.

### Setup

The first thing we need to decide on is whether or not we will be using
physical hardware for our analytical purposes or deploying virtual machines for
handling our malware specimens. The simplest and fastest way to get started is
usually virtualization.

I recommend one of two virtualization engines. VirtualBox or virt-manager in
conjunction with KVM VM's. Pick one. Your choice of virtual machine is
important but you can work successfully with just about any type of VM
provider.

You will need plenty of RAM and a large hard drive space. You may want to
strive to have 16 gigabytes or more RAM available on your system. However, it
is important to remember that in a virtualized environment, your malware may be
able to detect it is in a virtual machine. So don't rely entirely on a virtual
environment. Be prepared to deploy a piece of physical hardware if necessary.

### Isolation And Safety

1. Don't reuse your equipment. You should not keep personal data on your
   analytic machine.

2. Don't connect your laboratory and personal equipment to the same network.
   Malware is made to infect and to look for footholds. Don't provide one.

3. Use a dedicated DSL or Cable Modem for the connection of the system. Don't
   allow the malware in your lab to destroy someone elses system or to attack
   yours.

4. Speed up your work flow through the use of automation. Get low hanging
   fruit.

5. Take notes and keep ample documentation.

### Cuckoo

Cuckoo describes malware as the Swiss army knife of cyber criminals. I agree.
The adversary is always looking for a method by which they can gain a foothold
into our network, our life, and our wallet. It is important to understand that
we must be on the look out for all of the methods by which a plethora of
individuals can conduct an attack against us.

### Ponderings

1. What do I want to analyze?

2. How much analysis do I need to be able to do?

3. What platforms am I targeting?

4. What do I want to know?

We need to decide on operating system, language, patch level, and what software
we plan to have installed. Analysis of something like malware is as much art as
it is science. We need to take a sterile VM and make it look 'lived in'. If you
have no browser history and the malware is designed to hunt for browser
history, this will be an obvious issue.

We must never forget that our virtualized machines can be detected and
countermeasures against detection must be deployed if we want to get the
maximum amount of data from our tools.

### Virtualization Detection

Malware can use many tools to detect if it is inside of a virtualized
environment. A small list is provided.

1. Registry Checking and the presence of specific registry keys or searching
   for specific values such as 'VMware, Inc.' can potentially reveal the
   presence of virtualization.

2. Memory checking and verifying how memory structures are used. The Store
   Interrupt Descriptor Table is higher in a virtual environment than a
   physical machine. The malware might also look for Store Local Descriptor
   Table, Store Global Descriptor Table, and the Store Task Register.

3. Malware can run an IN instruction that is normally run from Ring 0. A VM
   will allow you to run IN from Ring 3 (The OS) and will return a value of
   'VMxh' to the EBX register. This confirms the existence of a VM.

4. A VMware MAC address can be presumed by a specific numbering.

5. Other hardware information can also reveal the existence of a VM.

Countering these issues requires intercepting file and process checks,
countering memory checks, and verifying that the OS is unable to communicate
with the VM. There also exist tools to assist with this like [vmcloak][vmcloak].

## Answers

1. Scanmem provides an automated method by which we can locate values through
   an interface exposed to the user. 

2. Scanmem provides the SET command to allow us to manipulate memory values.

3. Cuckoo can be used to automate malware analysis.

4. Malware can identify a VM through querying the registry in a Windows machine.

5. Malware analysis provides excellent intelligence on who is targeting us.


## Conclusion

Understanding your computer is paramount. Many users feel confidant in their
Linux skills but discover very quickly that their skill set is often lacking in
the basics. Memory, CPU, and all other low level aspects of the computer are
often obfuscated behind the operating system and much of this mysterious real
estate will never be explored by even some advanced users.

There exist plenty of tools that can be deployed to simplify your job and to
enhance your effectiveness. While some tools do increase complexity in one
realm, they can often have benefits that far outweigh the problems. Consider
using tools like Cuckoo to assist you in automating your malware scanning and
study. You may also consider learning how to follow an application through
execution so that you are better prepared in the event that you need to begin
dismantling or otherwise gaining insight into the tools being used to attack
your network.

Many users dispose of the malware that is being used to target them without a
second thought. I think this is a poor use of resources. Setting up even a basic
intelligence analysis group for your threats can enhance your safety and the
safety of others tremendously. Learn how to profile threat actors, automate and
triage attacks, and funnel that information towards law enforcement agencies
who can use that data to build cases and develop a course of action for
managing these threats.

## Final Recommendations

1. Use Linux.

2. Learn more about your computer.

3. Learn how to take notes.

4. Develop a plan for enhancing your skills.

5. Share Information. 

[gzdoom]: https://zdoom.org/downloads 'The GZDoom Player'
[scanmem]: https://github.com/scanmem/scanmem 'Memory Scanner'
[vmcloak]: http://vmcloak.org/ 'Help Cloak Your VM for Cuckoo'
