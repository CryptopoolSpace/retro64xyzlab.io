---
layout: post
title: "Raspberry Pi Game HAT"
date: 2018-10-20T04:52:43-07:00
author: RetroMe
summary: >
  The Wave Share Game HAT for the Raspberry Pi is an awesome accessory for any
  one interested in exploring emulation with their Raspberry Pi device.
categories: how-to
thumbnail: fa-desktop
tags:
 - raspberry pi
 - video games
 - review
 - emulation
 - how to
---

## Game HAT for Raspberry Pi

The [waveshare game HAT][waveshare] for the Raspberry Pi converts the device
into a classic game console. The device provides a 3.5inch IPS screen and
battery integration to allow on the go use. There is also an onboard speaker
and an earphone jack for the user. The device is a simple upgrade that costs
$39.99 and allows you to deploy your Raspberry Pi as a portable gaming system.

I found the device easy to install. You simple slip the Pi into the mount, add
a battery, and screw on the back to hold it all together. Installation required
about 10 minutes to get the hardware piece completed and an additional 10 to
copy the Pi HAT image to an SDCard and install. I found it extremely simple but
fun and satisfying.

I think the waveshare game HAT would make a great project for people new to
computing or just starting out with the maker scene. I think it would be great
for kids. I recommend it.

[waveshare]: https://www.waveshare.com/game-hat.htm 'The Pi Game HAT'
